
package roomreservation;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

//responds client requests, acts as an intermediary
public class Controller {
    private FileManager fm; //refers to singleto FileManager instance
    private Customer loggedCustomer = null; //holds currently logged customer
    
    public Controller()
    {
        //gets singleton instange of FileManager
        fm = FileManager.getFileManager();
    }
    
    //Given a new customer object, write it as a new record on Customer file
    //Only a single thread can access this method at a time
    //Enforces shared lock
    public synchronized Customer registerCustomer(Customer cus)
    {
        cus.setID(fm.getCount(fm.CUSTOMER));//sets customer id so that 
                                            //it's the final record
        fm.writeRecord(cus);//write customer object via file manager
        return cus;
    }
    
    //Given a room id, returns a room object while enficing a synchronised lock
    public Room getRoom(int id)
    {
        Room room = (Room)fm.readRecord(id,fm.ROOM); //read room record corresponding to id
        return room;
    }
    
    //Given a correct username and password, returns corresponding customer
    public  Customer login(String uname, String pass)
    {
        List list = fm.getAllRecords(fm.CUSTOMER);
        //chcecks all customer records for matching uname and password
        for(int i =0 ; i<list.size();i++)
        {
            Customer cus = (Customer)list.get(i);
            if(cus.checkLogin(uname, pass))
            {
                loggedCustomer = cus;//sets currntly logged in customer
                return loggedCustomer; //return corresponding customer
            }
        } 
        return null;
    }
    
    //Performs reservation when a new reservation details and a selected Room
    //is provided. Method enforces syncronised lock.
    public synchronized boolean reserve(Reservation res, Room rm)
    {
        
        boolean check = false; //to ensure successfull reservation
            Room room = (Room)fm.readRecord(rm.getID(),fm.ROOM);
            //ensure res or rm not refering to a null reference
            if((room != null) && (res!= null))
            {
                //cannot reserve if the room is already reserved
                if(room.isReserved(res))
                {
                    check =  false;
                }
                //otherwise
                else
                {
                    room.addReservation(res);//add resrvation to the room
                    fm.writeRecord(room);//write updated room record
                    check = true;
                }
            } 
        return check;//returns whether reservation is a success or not
    }
    
    //checks availability of a room, no syncronization needed because only
    //read requests are performed (controlled by read lock)
     public boolean isAvailable(Reservation res, Room rm)
    {
        Room room = new Room(0);
        room = (Room)fm.readRecord(rm.getID(),fm.ROOM);//reads given room
        if(room != null)
        {
            if(room.isReserved(res))
            {
                //room is not available if already reserved
                return false;
            }
            //otherwise
            else
            {
                return true;
            }
        }
        return false;
    }
     
    //Performs cancellation of a reservation when selected reservation
    //is provided. Method enforces syncronised lock .
    public synchronized boolean cancelReservation(Reservation res, Room rm)
    {
        boolean cancelled = true; //checks if cancellation is successfull
        Room room = (Room)fm.readRecord(rm.getID(),fm.ROOM); //get most recent update
        if(room.isReserved(res))
        {
                //cancellation possible only if room is reserved
                room.removeReservation(res);//remove reservaion from room
                fm.writeRecord(room);//write updated room record
                cancelled = true;
        }
        else
        {   
                 //cancellation not possible only if room isn't reserved
                cancelled = false;
        }
        return cancelled; //return cancellation status
    }
    
    //(A test method)Allows adding rooms to the Room file
    public synchronized void addNewRoom(Room room)
    {
        room.setID(fm.getCount(fm.ROOM));
        fm.writeRecord(room);
    }
   
}
