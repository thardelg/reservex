

package roomreservation;
import java.util.HashMap;
import java.util.Map;

public class ReadWriteLock {
    private Map<Thread, Integer> readers =
       new HashMap<Thread, Integer>();

   private int writers    = 0; //write acessing threads
   private int writeRequests    = 0; // read accessing threads
   private Thread currentWriter = null;


  public synchronized void lockRead() throws InterruptedException{
    Thread invokingThread = Thread.currentThread();
    while(! readable(invokingThread)){
      wait();
    }

    readers.put(invokingThread,
     (getReadAccessCount(invokingThread) + 1));
  }

  private boolean readable(Thread callingThread){
    if( isWriter(callingThread) ) return true;
    if( hasWriter()             ) return false;
    if( isReader(callingThread) ) return true;
    if( hasWriteRequests()      ) return false;
    return true;
  }


  public synchronized void unlockRead(){
    Thread invokingThread = Thread.currentThread();
    if(!isReader(invokingThread)){
      throw new IllegalMonitorStateException("Calling Thread does not" +
        " hold a read lock on this ReadWriteLock");
    }
    int accessCount = getReadAccessCount(invokingThread);
    if(accessCount == 1){ readers.remove(invokingThread); }
    else { readers.put(invokingThread, (accessCount -1)); }
    notifyAll();
  }

  public synchronized void lockWrite() throws InterruptedException{
    writeRequests++;
    Thread invokingThread = Thread.currentThread();
    while(! writable(invokingThread)){
      wait();
    }
    writeRequests--;
    writers++;
    currentWriter = invokingThread;
  }

  public synchronized void unlockWrite() throws InterruptedException{
    if(!isWriter(Thread.currentThread())){
      throw new IllegalMonitorStateException("Calling Thread does not" +
        " hold the write lock on this ReadWriteLock");
    }
    writers--;
    if(writers == 0){
      currentWriter = null;
    }
    notifyAll();
  }

  private boolean writable(Thread callingThread){
    if(isOnlyReader(callingThread))    return true;
    if(hasReaders())                   return false;
    if(currentWriter == null)          return true;
    if(!isWriter(callingThread))       return false;
    return true;
  }


  private int getReadAccessCount(Thread callingThread){
    Integer accessCount = readers.get(callingThread);
    if(accessCount == null) return 0;
    return accessCount.intValue();
  }


  private boolean hasReaders(){
    return readers.size() > 0;
  }

  private boolean isReader(Thread callingThread){
    return readers.get(callingThread) != null;
  }

  private boolean isOnlyReader(Thread callingThread){
    return readers.size() == 1 &&
           readers.get(callingThread) != null;
  }

  private boolean hasWriter(){
    return currentWriter != null;
  }

  private boolean isWriter(Thread callingThread){
    return currentWriter == callingThread;
  }

  private boolean hasWriteRequests(){
      return this.writeRequests > 0;
  }

}
