
package roomreservation;
import java.io.Serializable;

//Implementing Record Interface, hence represents a record in Customer File
public class Customer implements Record, Serializable{ 
    private int id=0;
    private String uname = "", pass = "", name = "", address = "", tel = "";
    public Customer(int id)
    {
        this.id = id;
    }
    public Customer(int id, String uname, String pass, String name, String address, String tel)
    {
         setID(id);
         setUname(uname);
         setPass(pass);
         setName(name);
         setAddress(address);
         setTel(tel);
    }
    public boolean checkLogin(String uname, String pass)
    {
        //checks if username and passwords match
        return (this.uname.equals(uname) && this.pass.equals(pass));
    }
    
    //mutators
    public void setID(int id)
    {
        this.id = id;
    }
    public void setUname(String uname)
    {
        this.uname = uname;    
    }
    public void setPass(String pass)
    {
        this.pass = pass;  
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public void setAddress(String address)
    {
        this.address = address;    
    }
    public void setTel(String tel)
    {
        this.tel = tel;
    } 
    
    //accessors
    public int getID()
    {
        return id;
    }
    public String getUname()
    {
        return uname;
    }
    public String getPass()
    {
        return pass;
    }
    public String getName()
    {
        return name;
    }
    public String Address()
    {
        return address;
    }
    public String getTel()
    {
        return tel;
    }
    
    //outputs a string with essential customer data
    public String toString() 
    {
        return "ID : " + id + "\nUsername : " + uname + "\nPassword : " 
                + pass + "\nName : " + name + "\nAddress : " + address 
                + "\nPhone : " + tel ; 
    }
    
}
