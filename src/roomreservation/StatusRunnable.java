
package roomreservation;

public class StatusRunnable implements Runnable{
    HotelRoomReservation hrr;
    public StatusRunnable(HotelRoomReservation hrr)
    {
        this.hrr = hrr;
    }
    public void run()
    {
        hrr.checkStatus();
    }
}
