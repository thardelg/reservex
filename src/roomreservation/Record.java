
package roomreservation;
//This interface represents a FileRecord having a unique ID
public interface Record {
     public int getID(); //returns ID of a FileRecord
}
