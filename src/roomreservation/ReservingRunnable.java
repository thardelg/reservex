
package roomreservation;

public class ReservingRunnable implements Runnable {
    HotelRoomReservation hrr;
    public ReservingRunnable(HotelRoomReservation hrr)
    {
        this.hrr = hrr;
    }
    public void run()
    {
        hrr.reserveRoom();
    }
}
