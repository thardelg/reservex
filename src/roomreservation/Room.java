
package roomreservation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//Implementing Record Interface, hence represents a record in Customer File
public class Room implements Record, Serializable{
    private int number;
    //Holds list of reservations on this room
    private List <Reservation> bookings = new ArrayList(); 
    public Room(int number)
    {
        this.number = number;
    }
    
    public int getID()
    {
        return number;
    }
    public void setID(int id)
    {
        this.number = id;
    }
    public List getBookings()
    {
        return bookings;
    }
    
    //given a reservation, checks if it is already reserved
     public boolean isReserved(Reservation newRes)
    {
        //gets start date of given reservation
        String start = Reservation.getLongDate(newRes.getStart());
        boolean reserved = false;
        
        //checks if all days of a given reservation are available
        for(Reservation res : bookings)
        {
            for(Date date : res.getReservedDays())
            {
                String check = Reservation.getLongDate(date);
                if(check.equals(start))
                {
                    reserved = true;
                    return reserved; //returns if at least one day is not available
                }
            }
        }
        return reserved;
    }
     
    public List<Reservation> getAllReservations()
    {
        return bookings; 
    }
    
    //gets all reservations made by a customer
    public List<Reservation> getReservationsByCustomer(Customer cus)
    {
       List list = new ArrayList();
       for(Reservation res : bookings)
       {
           //checks if the customer is the given customer of a reservation
           if(res.getCustomer().getID() == cus.getID())
           {
               list.add(res);
           }
       }
        return list;
    }
    
    //adds a reservaition to the reservations list if it is not reserved
    public synchronized boolean addReservation(Reservation res)
    {
        if(!isReserved(res))
        {
            bookings.add(res);
            return true;
        }
        return false;
 
    }
    
    //removes a resrvation from reservation list
    public synchronized boolean removeReservation(Reservation res)
    {
        if(isReserved(res))
        {
            for(Reservation oldRes : bookings)
            {
                //checks if starting dates of the given reservation
                //matches start day of an old reserved day
                String oldStart = Reservation.getLongDate(oldRes.getStart());
                String newStart = Reservation.getLongDate(res.getStart());
                if(oldStart.equals(newStart))
                {
                    bookings.remove(oldRes);
                    return true;
                }
                    
            }
        }
        return false;
    }
    
    //Returns details of all resrvations for the room as a String
    public String toString()
    {
         String out = "Reservations : \n";
         for(Reservation res : bookings)
         {
             out += res + "\n";
         }
         return out + "\nRoom No. : " + (number+1) ;
     }
}
