
package roomreservation;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
/**
 *
 * @author Tharindu Dananjaya
 */
public class RoomReservationDemo {

    public static void main(String[] args) throws IOException {
        
       /* try {
            Date dt = new Date();
            Thread.sleep(3000);
            Date dp  = new Date();
            DateFormat df = DateFormat.getDateInstance();
            System.out.println(df.format(dt).equals(df.format(dp)));
            } catch (InterruptedException ex) {
            Logger.getLogger(RoomReservationDemo.class.getName()).log(Level.SEVERE, null, ex);
        }*/
            try {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
                //com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel
                //com.sun.java.swing.plaf.gtk.GTKLookAndFeel
                //com.sun.java.swing.plaf.motif.MotifLookAndFeel
                //com.sun.java.swing.plaf.windows.WindowsLookAndFeel
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(RoomReservationDemo.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                Logger.getLogger(RoomReservationDemo.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(RoomReservationDemo.class.getName()).log(Level.SEVERE, null, ex);
            } catch (UnsupportedLookAndFeelException ex) {
                Logger.getLogger(RoomReservationDemo.class.getName()).log(Level.SEVERE, null, ex);
            } 
            
            LoginMenu mm = new LoginMenu();
            mm.setVisible(true);
            
//             Date t = new Date();
//             t.setTime(t.getTime()+ (86400000*6));
//            Customer cs = new Customer(2,"jana123","1234",
//                    "Janakshi Dulanga", "Colombo", "0777159268");
//            Reservation res = new Reservation(t, 1,cs);
//            Room room = new Room(2);
//            room.addReservation(res);
            //System.out.println(cs);
            //System.out.println("\n\n" + res);
            //System.out.println("\n\n" + room);
            
//            FileManager fm = FileManager.getFileManager();
//            fm.writeRecord(cs);
//            for(int i=0;i<10;i++)
//            {
//                fm.writeRecord(new Room(i));
//            }
            
            
//            FileManager fm = FileManager.getFileManager();
//            List k = fm.getAllRecords(fm.CUSTOMER);
//            for(int i =0 ; i<k.size();i++)
//            {
//                System.out.println(k.get(i));
//            } 
        
//            Controller cl = new Controller();
//             System.out.println(cl.login("tharindu123", "1234"));
//        FileManager fm = FileManager.getFileManager();
//        Record room = fm.readRecord(1,fm.ROOM);
//        System.out.println(room.getID());
       
//        Date date = new Date();
//        try {
//           
//            DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
//            date = df.parse("12/18/2012");
//            System.out.println(date);
//            System.out.println(Reservation.getLongDate(date));
//        } catch (ParseException ex) {
//            JOptionPane.showMessageDialog(null, "Invalid Date Format","Error", JOptionPane.INFORMATION_MESSAGE);
//            
//        }
        
       
}
}
