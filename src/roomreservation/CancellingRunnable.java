
package roomreservation;
public class CancellingRunnable implements Runnable {
    HotelRoomReservation hrr;
    public CancellingRunnable(HotelRoomReservation hrr)
    {
        this.hrr = hrr;
    }
    public void run()
    {
        hrr.cancelReservation();
    }
}
