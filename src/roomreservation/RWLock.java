
package roomreservation;
public class RWLock {

  private int readers       = 0; //reading thread count
  private int writers       = 0; //writers count (reaches maximum 1)
  private int writeRequests = 0; //count of threads waiting for writing

  //acquire read lock (shared lock)
  public synchronized void lockRead() throws InterruptedException{
    //calling thread waits if there is a writing thread or waiting writers
    while(writers > 0 || writeRequests > 0){
      wait();
    }
    //otherwise increment reading thread count
    readers++;
  }

  //release shared lock
  public synchronized void unlockRead(){
    readers--; //decrement reading threads
    notifyAll(); //wakes up all waiting threads on RWLock instance
  }

  //acquire write lock (exclusive lock)
  public synchronized void lockWrite() throws InterruptedException{
    //increment waiiting writers count
    writeRequests++;
    //calling thread waits if there are reading therads or a writing thread
    while(readers > 0 || writers > 0){
      wait();
    }
    //otherwise
    writeRequests--; //decrement waiting writers by one
    writers++; //increment writing thread count
  }

  //release exclusive lock
  public synchronized void unlockWrite() throws InterruptedException{
    //decrement writing thread count
    writers--;
    notifyAll(); //wakes up all waiting threads on RWLock instance
  }
}
