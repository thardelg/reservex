
package roomreservation;

public class RegisteringRunnable implements Runnable {
    HotelRoomReservation hrr;
    public RegisteringRunnable(HotelRoomReservation hrr)
    {
        this.hrr = hrr;
    }
    public void run()
    {
        hrr.registerCustomer();
    }
}
