
package roomreservation;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileManager {
                                            //indicate file path
    private final String PATH           = "C:\\Users\\Tharindu\\Desktop"
                                         + "\\Room_Reservation\\Data\\";
    private final String ROOMS_DATA     = "rooms.dat"; //file name saving Room data
    private final String CUSTOMER_DATA  = "customers.dat";//file name saving Customer data
    private String DATA_SOURCE = ROOMS_DATA; //default data source
    final static int ROOM = 0; //data Source type
    final static int CUSTOMER = 1;
    private static FileManager instance = null; //singleton instance
    private final ReadWriteLock rwLock = new ReadWriteLock(); //ReadWrite reentrant lock instance
     
    private FileManager(){} //only File Manager can construct its own object
    
    public static FileManager getFileManager()
    {
        //instantiate a new FileManager only if instance referes to null
        if(instance == null)
        {
            instance = new FileManager();
        }
        
        return instance;
    }
    private void switchSource(int type)
    {
         switch(type) //switch the data source according to the given type
        {
            case CUSTOMER : DATA_SOURCE = CUSTOMER_DATA; break;
            case ROOM     : DATA_SOURCE = ROOMS_DATA;    break;
            default       : DATA_SOURCE = ROOMS_DATA;
        }
    }
    
    //given a record id, read and return the FileRecord as a Record object
    public Record readRecord(int id, int type) 
    {
        try {
            rwLock.lockRead(); //aquire shared lock
            Object obj    = null;
            switchSource(type);//setect data source
            ObjectInputStream in = null;
            try
            {
                in = new ObjectInputStream( new FileInputStream(PATH + DATA_SOURCE));
                obj = in.readObject();
                
                //loops until record with id is found
                while(obj != null)
                {  
                   if(((Record)obj).getID() == id)
                   {
                       //if the record is found, read lock is released
                       rwLock.unlockRead();
                       return (Record)obj; //returns found record
                   }
                   obj = in.readObject();
                }
            }
            
            //catch any end of file exception
             catch(EOFException e)
            {
                System.out.println("EXCEPTION : " + e);
            }
            catch(IOException e)
            {
                System.out.println("EXCEPTION : " + e);
            }
            catch(ClassNotFoundException e)
            {
                 System.out.println("EXCEPTION : " + e);
            }
            finally
            {
                if(in != null) try {
                    in.close();//Finally closes input stream reader
                } catch (IOException ex) {
                    Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            } 
        } catch (InterruptedException ex) {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        //if Record is not found, relese readLock
        rwLock.unlockRead();
        return null;
    }
    
    //read all records in the file
    public List getAllRecords(int type)
    {
        List list = new ArrayList();
        switchSource(type);
        Record rec = null;
        rec = readRecord(0,type);//read first record
        //read until record refers to null object(until EOF)
        for(int i=1; rec != null ; i++)
        {
            list.add(rec);//add each record to a list
            rec = readRecord(i,type);   //read each record
        }
        return list;
    }
    
    //write a given record to the file
    public void writeRecord(Record record)
    {
        try
        {
            
            int type = this.ROOM;
            String clsName = record.getClass().getSimpleName();
            //determinte type of record (Customer or Room) bu class name
            if (clsName.equals( "Customer"))
            {
               type = this.CUSTOMER;
            }
            else if (clsName.equals( "Room"))
            {
               type = this.ROOM;
            }
            
            List list = getAllRecords(type); //get all records
            ObjectOutputStream out = null;
            try
            {
                rwLock.lockWrite();//acquire  exclusive lock on file
                out = new ObjectOutputStream(new FileOutputStream(PATH + DATA_SOURCE));
                if(list.size()== 0)
                {
                    //if no object is in file, write as first record
                    out.writeObject(record);
                }
                else 
                {
                     for(int i=0; i < list.size();i++)
                    {
                        //if record is found, replace with new one
                        if(i == record.getID())
                        {
                            out.writeObject(record);
                        }
                        //if record not found, add record at the end
                        else
                        {
                            out.writeObject(list.get(i));
                        }
                    }
                } 
                
            }
              catch(EOFException e)
            {
                System.out.println("EXCEPTION : " + e);
            }
            catch(IOException e)
            {
                System.out.println("EXCEPTION : " + e);
            }
            finally
            {
                //finally release exclusive lock
                rwLock.unlockWrite(); 
                if(out != null) try {
                    out.close();
                } catch (IOException ex) {
                    Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        catch(InterruptedException ex)
        {
            Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
       

    }
    
    //get total count of records in a file
    public int getCount(int type)
    {
        return getAllRecords(type).size();
    }
    
}
