
package roomreservation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//FileManager writes Reservation objects using binary IO
public class Reservation implements Serializable{
    private List <Date> reservedDays = new ArrayList();//holds all reserved days 
                                                       //for one reservation
    private BigDecimal cost = new BigDecimal(0.0);//To avoid miscalculations
    private Customer cus;
    //Gets date of arrival and length of stay to construct a new reservation
    public Reservation(Date arrival, int lengthOfStay, Customer cus)
    {
        reservedDays.add(arrival);//adds first day to reserved days list
        cost = new BigDecimal(3250); //sets reservation cost
        for(int i=1;i < lengthOfStay; i++)
        {
            Date t = new Date(); 
            //increment currnet date of t by one
            t.setTime(reservedDays.get(i-1).getTime()+ 86400000);
            cost = cost.add(new BigDecimal(3250.0));
            reservedDays.add(t);
        }
        this.cus = cus;
    }
    public List<Date> getReservedDays()
    {
        return reservedDays;
    }
    public BigDecimal getCost()
    {
        return cost;
    }
    
    public static String getLongDate(Date date)
    {
        DateFormat dateFormatter; //Formats date to the Long date format
        dateFormatter = DateFormat.getDateInstance(DateFormat.LONG);
        return dateFormatter.format(date);
    }
    public Date getStart()
    {
        return reservedDays.get(0); //returns first reserved day
    }
    public Date getEnd()
    {
        //returns last reserved day
        return reservedDays.get(reservedDays.size()-1);
    }
    public Customer getCustomer()
    {
        return this.cus;
    }
    
    //outputs essential data of a reservation
    public String toString() 
    {
        String dates = "Customer>>>\n" + cus + "\n\nReserved Dates>>>\n";
        for(int i=0;i < reservedDays.size(); i++) //prints all reserved days
        {
            dates += getLongDate(reservedDays.get(i)) + "\n" ;
        }
        return dates + "\nTotal Cost : Rs." + cost.toString();
    }
    
}
