# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

A Java virtual machine and its underlying operating system (OS) provide mappings from apparent simultaneity to physical parallelism (via multiple CPUs), or lack thereof, by allowing independent activities to proceed in parallel when possible and desirable, and otherwise by time sharing.

The project looks into design and implementation of a concurrent program, by using appropriate concurrent programming constructs and design methodologies. The assignment requires successful implementation of a concurrent program by following a suitable design made of diagramming techniques like LTS, structure diagrams and FSP. 


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact